namespace Singleton
{
    public class Settings
    {
        private Settings() {}

        private static class SettingsHolder
        {
            internal static readonly Settings Settings = new Settings();
        }

        public static Settings GetInstance()
        {
            return SettingsHolder.Settings;
        }
    }
}